<?php

namespace Siliconrockstar\Jsmodule\Component;

class Uicomponent4 extends \Magento\Ui\Component\AbstractComponent
{

    const NAME = 'html_content_siliconrockstar_jsmodule';

    public function getComponentName() {
        return self::getName();
    }

}
