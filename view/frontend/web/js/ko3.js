define(['uiComponent', 'ko'], function (Component, ko) {
    "use strict";

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            console.log(Component);
            this._super();
            this.othertextvalue = 'value from ko3 JS Module constructor';
            /* make textvalue an observable */
            this.textvalue = ko.observable([this.textvalue]);
            
        },
        updateTextValue: function() {
            /* update the observable 'textvalue' */
            this.textvalue('new value from JS Module function ' + Date.now()); 
        }
    });

});
