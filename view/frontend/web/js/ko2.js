define(['uiComponent'], function (Component) {
    "use strict";

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.othertextvalue = 'value from JS Module constructor';

        }
    });

});
