define(['uiElement', 'ko'], function (Element, ko) {
    "use strict";

    return Element.extend({
        defaults: {
            template: 'Siliconrockstar_Jsmodule/uicomponent4'
        }
    });

});
