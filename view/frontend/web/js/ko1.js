define(['uiComponent'], function (Component) {
    "use strict";

    return Component.extend({
        /** @inheritdoc */
        initialize: function () {
            this._super();
            this.value = 'value from module';

        }
    });

});
