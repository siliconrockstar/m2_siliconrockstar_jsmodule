define(['ko'], function (ko) {
    "use strict";
    
    return function(config, node) {
        
        console.log('The passed in value is ' + config.dataValue);
        
        // set the node's text content equal to the passed in value
        node.textContent=config.dataValue;
     
    };

});
