define([], function () {
    /* this var name can be whatever I think */
    var srAlert = function (config, node) {
        alert(config.message);
        console.log('srAlert function called: ' + config.message);
    }
    return srAlert;
});
