<?php
namespace Siliconrockstar\Jsmodule\Api;

use Siliconrockstar\Jsmodule\Api\Data\NameInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface NameRepositoryInterface 
{
    public function save(NameInterface $page);

    public function getById($id);

    public function getList(SearchCriteriaInterface $criteria);

    public function delete(NameInterface $page);

    public function deleteById($id);
}
