<?php
namespace Siliconrockstar\Jsmodule\Model;
class Name extends \Magento\Framework\Model\AbstractModel implements \Siliconrockstar\Jsmodule\Api\Data\NameInterface, \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'siliconrockstar_jsmodule_name';

    protected function _construct()
    {
        $this->_init('Siliconrockstar\Jsmodule\Model\ResourceModel\Name');
    }

    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }
}
