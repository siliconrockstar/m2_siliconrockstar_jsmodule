<?php
namespace Siliconrockstar\Jsmodule\Model\ResourceModel;
class Name extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('siliconrockstar_jsmodule_name','name_id');
    }
}
