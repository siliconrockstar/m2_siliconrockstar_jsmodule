<?php
namespace Siliconrockstar\Jsmodule\Model\ResourceModel\Name;
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init('Siliconrockstar\Jsmodule\Model\Name','Siliconrockstar\Jsmodule\Model\ResourceModel\Name');
    }
}
