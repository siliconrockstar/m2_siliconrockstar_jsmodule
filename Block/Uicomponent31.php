<?php

namespace Siliconrockstar\Jsmodule\Block;

use Magento\Framework\View\Element\BlockInterface;

class Uicomponent31 extends \Magento\Framework\View\Element\AbstractBlock
{

    public function toHtml() {
        return '<p>This content came from server-side Block class at ./Block/Uicomponent31.php, included via layout file '
        . './view/frontend/layout/siliconrockstar_jsmodule_index_uicomponent3.xml. This content was rendered at the server.</p>';
    }

}
