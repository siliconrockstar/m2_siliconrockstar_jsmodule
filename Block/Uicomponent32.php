<?php

namespace Siliconrockstar\Jsmodule\Block;

use Magento\Framework\View\Element\BlockInterface;

class Uicomponent32 extends \Magento\Framework\View\Element\AbstractBlock
{

    public function toHtml() {
        return '<p>This content came from server-side Block class at ./Block/Uicomponent32.php, included via UI component config file '
        . './view/frontend/ui_component/sr_uicomponentn3.xml. This content was delivered within an x-magento-init tag and'
                . ' then displayed on the page via a knockout getTemplate() call.</p>';
    }

}
